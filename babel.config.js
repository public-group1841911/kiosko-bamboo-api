module.exports = {
   presets: [
      ['@babel/preset-env', { targets: { node: 'current' } }],
      '@babel/preset-typescript'
   ],
   plugins: [
      [
         'module-resolver',
         {
            extensions: ['.js', '.ts', '.json'],
            alias: {
               shared: ['./src/shared']
            }
         }
      ]
   ]
   // ignore: ['**/*.spec.ts']
};
